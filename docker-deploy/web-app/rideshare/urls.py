from django.contrib import admin
from django.urls import path, re_path
from . import views

app_name = 'rideshare'

urlpatterns = [
    path('', views.login),
    path('index/<int:pk_>/', views.index),
    path('login/', views.login),
    path('register/', views.register),
    path('logout/', views.logout, name="logout"),
    path('index/<int:pk_>/becomedriver/', views.becomedriver),
    path('index/<int:pk_>/userprofile/', views.userprofile),
    path('index/<int:pk_>/riderequest/', views.riderequest),
    path('index/<int:pk_>/ridejoin/', views.ridejoin), # request web for a join
    path('index/<int:pk_>/ridejoin/join_a_ride/<int:pk>/<int:headcount>/', views.join_a_ride, name='join_a_ride'),
    path('index/<int:pk_>/myrides/', views.myrides),
    path('index/<int:pk_>/ordertake/', views.ordertake), #for a drivee to take a order
    path('index/<int:pk_>/myopenrides/', views.myopenrides),
    path('index/<int:pk_>/myopenrides/<int:pk>/edit/', views.editRideRequest.as_view()),
    path('index/<int:pk_>/myopenrides/<int:pk>/edit/success/', views.editSuccess),
    path('index/<int:pk_>/myconfirmedrides/', views.myconfirmedrides),
    path('index/<int:pk_>/openpickup/', views.openpickup),
    path('index/<int:pk_>/openpickup/<int:pk>/pick/', views.takepickup),
    path('index/<int:pk_>/myconfirmedpickup/', views.myconfrimedpickups),
    path('index/<int:pk_>/myconfirmedpickup/<int:pk>/complete/', views.completepickup),
]
