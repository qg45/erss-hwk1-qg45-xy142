from django.db import models

# Create your models here.


class User(models.Model):
    NORMAL = 'N'
    DRIVER = 'D'
    ROLES = [(NORMAL, 'Normal'), (DRIVER, 'Driver')]

    email = models.EmailField(unique=True)
    name = models.CharField(max_length=128)
    password = models.CharField(max_length=256)
    role = models.CharField(max_length=1, choices=ROLES, default=NORMAL)

    def __str__(self):
        return self.name


class DriverCar(models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=128)
    platenum = models.CharField(max_length=256)
    cartype = models.CharField(max_length=256)
    capacity = models.PositiveIntegerField()
    specialinfo = models.CharField(max_length=256, default='')


class Order(models.Model):
    OPEN = 'O'
    CONFIRMED = 'CF'
    CLOSE = 'CL'
    STATUS = [(OPEN, 'open'), (CONFIRMED, 'confirmed'), (CLOSE, 'close')]

    user = models.ForeignKey(verbose_name='User', to=User,
                             on_delete=models.CASCADE, default=None, blank=True, null=True)
    drivercar = models.ForeignKey(verbose_name='DriverCar', to=DriverCar,
                                  on_delete=models.CASCADE, default=None, blank=True, null=True)
    arrivaltime = models.DateTimeField('Expected Arrival')
    dest = models.TextField('Destination', max_length=400)
    status = models.CharField('Status', max_length=2,
                              choices=STATUS, default=OPEN)
    sharable = models.BooleanField('Share ride or not')
    headcount = models.PositiveIntegerField()
    specialreq = models.CharField(max_length=256, default='', blank=True)


class SharedOrder(models.Model):
    order = models.ForeignKey(verbose_name='Order',
                              to=Order, on_delete=models.CASCADE)
    user = models.ForeignKey(
        verbose_name='User', to=User, on_delete=models.CASCADE)
    #add a number
    headcount = models.PositiveIntegerField(default=1)
