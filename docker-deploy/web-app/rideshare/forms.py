from django import forms
from . import models
from . import utils


class RequestForm(forms.ModelForm):
    arrivaltime = forms.DateTimeField(
        label="(month/day/year hh:mm:ss)",
        widget=utils.DateSelectorWidget
    )

    class Meta:
        model = models.Order
        fields = ['dest', 'sharable', 'arrivaltime', 'headcount', 'specialreq']

class SharerRequestForm(forms.ModelForm):
    earliest_arrivaltime = forms.DateTimeField(
        label="(month/day/year hh:mm:ss)",
        widget=utils.DateSelectorWidget
    )

    lastest_arrivaltime = forms.DateTimeField(
        label="(month/day/year hh:mm:ss)",
        widget=utils.DateSelectorWidget
    )

    class Meta:
        model = models.Order
        fields = ['dest', 'sharable', 'headcount', 'specialreq']
