from email import message
import email
from email.policy import default
from django.shortcuts import get_object_or_404, render
from django.shortcuts import redirect
from django.core.validators import validate_email
from . import models
from . import utils
from . import forms
from django.views import generic
from datetime import datetime
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail

# Create your views here.

def index(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        return render(request, 'rideshare/structs/index.html', locals())
    except KeyError:
        return redirect('/login/')


def login(request):
    if request.method == "POST":
        email = request.POST.get('email')
        password = request.POST.get('password')
        message = 'Please check your input'
        if email.strip() and password:
            if not utils.validateEmail(email):
                message = 'Email address is not following traditional format.'
                return render(request, 'rideshare/structs/login.html', {'message': message})
            try:
                user = models.User.objects.get(email=email)
            except:
                message = 'Your email address does not exist'
                return render(request, 'rideshare/structs/login.html', {'message': message})

            if user.password == password:
                request.session['email'] = user.email
                request.session['name'] = user.name
                request.session.set_expiry(999999999)
                pk = user.pk
                return redirect('/index/'+str(pk)+'/')
            else:
                message = 'Wrong password!'
                return render(request, 'rideshare/structs/login.html', {'message': message})
        else:
            return render(request, 'rideshare/structs/login.html', {'message': message})
    # if not POST type do nothing
    return render(request, 'rideshare/structs/login.html')


def register(request):
    if request.method == 'POST':
        message = "Please double check what eamil you wrote and password can not be none!"
        username = request.POST.get('username')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        email = request.POST.get('email')
        if email.strip() and password1:
            if not utils.validateEmail(email):
                message = 'Email address is not following traditional format.'
                return render(request, 'rideshare/structs/register.html', {'message': message})

            if password1 != password2:
                message = "The two passwords are not same!"
                return render(request, 'rideshare/structs/register.html', locals())
            else:
                same_email_user = models.User.objects.filter(email=email)
                if same_email_user:
                    message = "The email has been already used!"
                    return render(request, 'rideshare/structs/register.html', locals())

                new_user = models.User()
                new_user.name = username
                new_user.password = password1
                new_user.email = email
                new_user.save()
                print("create successfully")
                return redirect('/login/')
        else:
            return render(request, 'rideshare/structs/register.html', locals())
    # if not POST Do nothing
    return render(request, 'rideshare/structs/register.html', locals())


def logout(request, *args, **kwargs):
    try:
        del request.session['email']
        del request.session['name']
    except KeyError:
        pass
    return redirect("/login/")


def becomedriver(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        if request.method == "GET":
            return render(request, 'rideshare/structs/becomedriver.html', locals())
        if request.method == "POST":
            email = request.session['email']
            name = request.session['name']
            platenum = request.POST.get('platenum')
            cartype = request.POST.get('cartype')
            capacity = int(request.POST.get('capacity'))
            specialinfo = request.POST.get('specialinfo')

            if capacity < 0:
                message = 'Your capacity input is not valid'
                return render(request, 'rideshare/structs/becomedriver.html', locals())

            user = models.User.objects.get(email=email)
            user.role = 'D'
            user.save()
            new_drivercar = models.DriverCar()
            new_drivercar.email = email
            new_drivercar.name = name
            new_drivercar.platenum = platenum
            new_drivercar.cartype = cartype
            new_drivercar.capacity = capacity
            new_drivercar.specialinfo = specialinfo
            new_drivercar.save()
            return redirect('/index/' + str(user.pk) + '/')
    except KeyError:
        return redirect('/login/')
    return render(request, 'rideshare/structs/becomedriver.html')


def userprofile(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        if user.role == 'D':
            driver = models.DriverCar.objects.get(email=user.email)
        if request.method == 'POST':
            # upadating information
            username = request.POST.get('username')
            password = request.POST.get('password')
            user.name = username
            user.passowrd = password
            user.save()
            if user.role == 'D':
                email = request.session['email']
                platenum = request.POST.get('platenum')
                cartype = request.POST.get('cartype')
                capacity = int(request.POST.get('capacity'))
                specialinfo = request.POST.get('specialinfo')
                driver.email = email
                driver.platenum = platenum
                driver.cartype = cartype
                driver.capacity = capacity
                driver.specialinfo = specialinfo
                driver.save()
            message = "You change your profile successfully!"
            return render(request, 'rideshare/structs/userprofile.html', locals())
        elif request.method == 'GET':
            return render(request, 'rideshare/structs/userprofile.html', locals())
    except KeyError:
        return redirect('/login')


def riderequest(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        if request.method == 'GET':
            orderform = forms.RequestForm()
            message = "let's go!"
            return render(request, 'rideshare/structs/riderequest.html',
                          {'orderform': orderform, 'message': message, 'user': user})
        if request.method == 'POST':
            orderform = forms.RequestForm(request.POST)
            if orderform.is_valid():
                form = orderform.save(commit=False)
                form.user = user
                form.save()
                return redirect('/index/' + str(user.pk) + '/')
            else:
                message = "Refill your ride request"
                return render(request, 'rideshare/structs/riderequest.html',
                              {'orderform': orderform, 'message': message})
    except KeyError:
        return redirect('/login')


def ridejoin(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        if request.method == 'GET':
            return render(request, 'rideshare/structs/ridejoin.html', {'user' : user})
        if request.method == 'POST':
            destnation = request.POST.get('destnation')
            earliest_arrivaltime = request.POST.get('earliest_arrivaltime')
            lastest_arrivaltime = request.POST.get('lastest_arrivaltime')
            headcount = request.POST.get('headcount')

            earliest_arrivaltime =datetime.strptime(earliest_arrivaltime,"%Y-%m-%dT%H:%M")
            lastest_arrivaltime =datetime.strptime(lastest_arrivaltime,"%Y-%m-%dT%H:%M")
            earliest_arrivaltime =earliest_arrivaltime.astimezone(timezone.utc)
            lastest_arrivaltime =lastest_arrivaltime.astimezone(timezone.utc)
            ridesfitted = models.Order.objects.filter(
            status='O').filter(
            sharable=True).filter(
            dest= destnation).filter(
            arrivaltime__range=(earliest_arrivaltime,lastest_arrivaltime))
            count = ridesfitted.count()
            if count ==0:
                message = "Currently there is no existed rides satisfy your requirement"
            message ="Great! We find "+ str(count) +" rides available for your to join!"
            return render(request, 'rideshare/structs/rideshared.html',
            {'ridesfitted':ridesfitted, 'message':message,'headcount':headcount, 'user' : user})

    except KeyError:
        return redirect('/login')


def join_a_ride(request,pk_,pk,headcount):
    order = models.Order.objects.get(pk =pk)
    user = models.User.objects.get(email=request.session['email'])
    # add headcount
    # add new entries in sharer
    new_shareorder = models.SharedOrder()
    new_shareorder.order = order
    new_shareorder.user = user
    new_shareorder.headcount = headcount
    new_shareorder.save()
    print("new share order create successfully")
    message = "You successfully joined in that ride!"
    return render(request, 'rideshare/structs/join_a_ride.html', {'message':message, 'user': user})

def myrides(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        return render(request, 'rideshare/structs/myrides.html',{'user':user})
    except KeyError:
        return redirect('/login')

def myopenrides(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        open_orders = []
        open_orders_temp =  models.Order.objects.filter(user=user).filter(status=models.Order.OPEN)
        # add shared
        shared_orders = models.SharedOrder.objects.filter(user=user)
        open_orders.extend(open_orders_temp)
        for shared_order in shared_orders:
            open_orders.append(shared_order.order)
        return render(request, "rideshare/structs/myopenrides.html", locals())
    except KeyError:
        return redirect('/login')

class editRideRequest(generic.UpdateView):
    model = models.Order
    form_class = forms.RequestForm
    template_name = 'rideshare/structs/riderequestedit.html'
    success_url = 'success/'
    messages = "Order is edited"

    def get_object(self):
        return self.model.objects.get(pk=self.kwargs['pk'])

def editSuccess(request, *args,**kwargs):
    order = models.Order.objects.get(pk=kwargs['pk'])
    if order.sharable:
        sharers = models.SharedOrder.objects.filter(order=order)
        passengers = []
        for sharer in sharers:
            passengers.append(sharer.user.email)
            send_mail(
                subject='Ride Cancelled',   # subject
                message='Your order is cancelled as owner edit the order',   # message
                from_email=settings.EMAIL_HOST_USER,   # from email
                recipient_list= passengers,  # to email testing
                fail_silently=False,
            )
            sharer.delete()
    message = "You've successfully editted an order."
    return render(request, "rideshare/structs/myrides.html", {'message':message})
        

def myconfirmedrides(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        confirmed_orders = models.Order.objects.filter(user=user).filter(status=models.Order.CONFIRMED)
        return render(request, 'rideshare/structs/myconfirmedrides.html', locals())
    except KeyError:
        return redirect('/login')

def ordertake(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        return render(request, 'rideshare/structs/mypickup.html', locals())
    except KeyError:
        return redirect('/login')

def openorder(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        return render(request, 'rideshare/structs/mypickup.html', locals())
    except KeyError:
        return redirect('/login')

def openpickup(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        drivercar = models.DriverCar.objects.get(email=user.email)
        capacity = drivercar.capacity
        # add filter
        open_orders_temp = models.Order.objects.filter(status=models.Order.OPEN)
        open_orders = []
        for open_orders_t in open_orders_temp:
            total = open_orders_t.headcount
            for sharer in models.SharedOrder.objects.filter(order=open_orders_t):
                total += sharer.headcount
            if total <= capacity:
                open_orders.append(open_orders_t)
        return render(request, 'rideshare/structs/openpick.html', locals())
    except KeyError:
        return redirect('/login')

def takepickup(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        # send msg
        passengers = []
        order = models.Order.objects.get(pk=kwargs['pk'])
        passengers.append(order.user.email)
        sharers = models.SharedOrder.objects.filter(order=order)
        for sharer in sharers:
            passengers.append(sharer.user.email)
        send_mail(
            subject='Ride Confirmation -- Ride Sharing Service',   # subject
            message='Your order is confirmed',   # message
            from_email=settings.EMAIL_HOST_USER,   # from email
            recipient_list= passengers,  # to email testing
            fail_silently=False,
        )
        #order status
        order.status = models.Order.CONFIRMED
        order.drivercar = models.DriverCar.objects.get(email=request.session['email'])
        order.save()
        message = "You have successfully confirmed an order!"
        return render(request, "rideshare/structs/mypickup.html", {'message': message, 'user': user})
    except KeyError:
        return redirect('/login')

def myconfrimedpickups(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        drivercar = models.DriverCar.objects.get(email=user.email)
        confirmed_orders = models.Order.objects.filter(drivercar=drivercar).filter(status=models.Order.CONFIRMED)
        return render(request, 'rideshare/structs/myconfirmedpickups.html', locals())
    except KeyError:
        return redirect('/login')

def completepickup(request, *args, **kwargs):
    try:
        user = models.User.objects.get(email=request.session['email'])
        drivercar = models.DriverCar.objects.get(email=user.email)
        order = models.Order.objects.get(pk=kwargs['pk'])
        order.status = models.Order.CLOSE
        order.save()
        confirmed_orders = models.Order.objects.filter(drivercar=drivercar).filter(status=models.Order.CONFIRMED)
        return render(request, 'rideshare/structs/myconfirmedpickups.html', locals())
    except KeyError:
        return redirect('/login')